#!/usr/bin/env sh

set -e

export ENV=test

. ./build/setup.sh

docker-compose run --rm python sh -c "coverage run ./manage.py test && coverage xml"
docker-compose run --rm npm-dev-server npm test -- --coverage
