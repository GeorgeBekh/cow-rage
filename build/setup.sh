export USER_ID=$(id -u)
export GROUP_ID=$(id -g)

export POSTGRES_PASSWORD=admin
export POSTGRES_DB=app

docker-compose build python

if [ "$ENV" != prod ]
then
    export COMPOSE_FILE=docker-compose.yml:docker-compose.dev.yml:docker-compose.ports.dev.yml

    cp ./cowrage/settings_dev.py ./cowrage/settings.py

    docker-compose run --rm npm-builder npm i

    docker-compose build python
else
    export COMPOSE_FILE=docker-compose.yml:docker-compose.ports.prod.yml

    cp ./cowrage/settings_base.py ./cowrage/settings.py

    docker-compose run --rm npm-builder sh -c "npm i && npm run build"

    docker-compose build
fi
