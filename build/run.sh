#!/usr/bin/env sh

. ./build/setup.sh

docker-compose up -d --force-recreate
docker-compose logs -f
