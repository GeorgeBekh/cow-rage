# cow-rage

Self-hosted web tool for collecting and tracking change in code coverage across a project (git repo)

## Roadmap for v1.0

Functional:

- [ ] Ability to add a project from **gitlab** or **github**
- [ ] Show coverage in percents for each individual commit in base branch
- [ ] Simple setup for clover coverage reports
- [ ] Show coverage difference with base branch in **pull-request** checks

DevOps:

- [ ] Publish docker image with this project to **dockerhub**
- [ ] Simple docker-compose config for "one-command" setup on server

Development:

- [ ] Ability to develop this project using docker
