from django.db import models
from functools import partial
from .utils import random

class Project(models.Model):
    name = models.CharField(max_length = 50)
    token = models.CharField(max_length=40, default=partial(random.string, 40), unique=True)
    base_branch = models.CharField(max_length=100, default='master')
    access_token = models.CharField(max_length=100, default='')

    def __str__(self):
        return self.name

class Branch(models.Model):
    name = models.CharField(max_length = 100)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)

    class Meta:
        unique_together = (('name', 'project'),)

    def __str__(self):
        return self.name

class Commit(models.Model):
    commit_id = models.CharField(max_length = 40)
    date_added = models.DateTimeField(auto_now_add=True)
    branch = models.ForeignKey('Branch', null=True, on_delete=models.CASCADE)
    coverage = models.FloatField()

    class Meta:
        unique_together = (('commit_id', 'branch'),)

    def __str__(self):
        return self.commit_id
