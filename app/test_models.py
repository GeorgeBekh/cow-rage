from django.test import TestCase
from .models import Project

class ProjectTestCase(TestCase):
    def setUp(self):
        Project.objects.create(name="Project1", token="someToken")
        Project.objects.create(name="Project2", token="someOtherToken")

    def test_converts_to_string_correctly(self):
        project1 = Project.objects.get(name='Project1')
        project2 = Project.objects.get(name='Project2')
        self.assertEqual(str(project1), 'Project1')
        self.assertEqual(str(project2), 'Project2')
