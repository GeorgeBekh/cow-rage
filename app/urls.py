from django.conf.urls import include, url
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'projects', views.ProjectViewSet)

urlpatterns = [
    url(r'^upload-report', views.upload_report),
    url(r'^', include(router.urls)),
]
