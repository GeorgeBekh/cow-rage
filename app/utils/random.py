import secrets
from string import ascii_letters

def string(length):
   letters = ascii_letters
   return ''.join(secrets.choice(letters) for i in range(length))
