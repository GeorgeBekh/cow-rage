import requests
import logging

class StatusPublisherFactory:
    def __init__(
            self,
            access_token,
            project_id,
            commit_id,

    ):
        self.access_token = access_token
        self.project_id = project_id
        self.commit_id = commit_id

    def create(self):
        if not self.access_token:
            return None

        return GitLabStatusPublisher(
            self.access_token,
            self.project_id,
            self.commit_id
        )

class GitLabStatusPublisher:
    def __init__(
            self,
            access_token,
            project_id,
            commit_id
    ):
        self.access_token = access_token
        self.project_id = project_id
        self.commit_id = commit_id

    def set_commit_status(self, state, coverage, coverage_diff):
        logging.error(coverage_diff)
        response = requests.post(
            'https://gitlab.com/api/v4/projects/' + self.project_id + '/statuses/' + self.commit_id,
            data={
                'state': state,
                'name': 'coverage',
                'target_url': 'http://example.com',
                'coverage': coverage,
                'description': "%.2f, \nDifference with base: %.2f" % (coverage, coverage_diff)
            },
            headers={'Private-Token': self.access_token}
        )
        logging.error(response.text)
