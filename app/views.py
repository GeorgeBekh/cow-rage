import logging
from rest_framework.decorators import api_view, permission_classes, parser_classes
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser
from .models import Project, Branch, Commit
from .serializers import ProjectSerializer
from rest_framework import viewsets, permissions
from xml.etree import ElementTree
from django.db.utils import IntegrityError
from .utils import publishing

class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

BASE_BRANCH = 'master'

@api_view(['PUT'])
@permission_classes((permissions.AllowAny,))
@parser_classes((MultiPartParser,))
def upload_report(request):
    token = request.META.get('HTTP_X_PROJECT_TOKEN', None)
    branch_name = request.data.get('branch', None)
    commit_id = request.data.get('commit', None)
    gitlab_project_id = request.data.get('project_id', None)
    file_obj = request.FILES.get('file', None)

    project = Project.objects.filter(token=token).first()

    if file_obj == None or project == None or gitlab_project_id == None:
        return Response(status=400)

    branch = get_branch(branch_name, project)
    if branch_name == BASE_BRANCH and branch == None:
        branch = create_branch(branch_name, project)

    coverage = calculate_coverage(request.FILES['file'].read())
    diff = coverage - get_branch_coverage(BASE_BRANCH, project)

    status_publisher = publishing.StatusPublisherFactory(
        access_token=project.access_token,
        project_id=gitlab_project_id,
        commit_id=commit_id
    ).create()

    if status_publisher:
        status_publisher.set_commit_status('success', coverage, diff)

    if branch_name == BASE_BRANCH:
        try:
            save_coverage(coverage, branch, commit_id)
        except IntegrityError:
            return Response("Commit have been already processed", 201)

    return Response("Diff: " + str(diff), 201)

def calculate_coverage(xml):
    report = ElementTree.fromstring(xml)
    line_coverage = report.attrib.get('line-rate', None)

    if line_coverage == None:
        return None

    return float(line_coverage) * 100

def get_branch(name, project):
    return Branch.objects.filter(name=name, project=project).first()

def create_branch(name, project):
    return Branch.objects.create(name=name, project=project)

def save_coverage(coverage, branch, commit_id):
    Commit.objects.create(commit_id=commit_id, branch=branch, coverage=coverage)

def get_branch_coverage(branch_name, project):
    branch = Branch.objects.filter(name=branch_name, project=project).first()
    last_commit = Commit.objects.filter(branch=branch).order_by('-date_added').first()
    if last_commit != None:
        return last_commit.coverage

    return 0
