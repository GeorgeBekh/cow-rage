from django.contrib import admin

from .models import Project, Branch, Commit

admin.site.register(Project)

# dev
admin.site.register(Branch)
admin.site.register(Commit)
