#!/usr/bin/env sh

COMMIT=$CI_BUILD_REF
BRANCH=$CI_COMMIT_REF_NAME
PROJECT_ID=$CI_PROJECT_ID

curl -v -X PUT -H "X-Project-Token: ${PROJECT_TOKEN}" \
     -F "branch=${BRANCH}" \
     -F "commit=${COMMIT}" \
     -F "file=@${PWD}/coverage.xml" \
     -F "project_id=${PROJECT_ID}" \
     ${SERVICE_ORIGIN:-http://localhost:3000}/api/upload-report
