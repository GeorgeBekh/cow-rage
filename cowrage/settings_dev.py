from .settings_base import *

AUTH_PASSWORD_VALIDATORS = []
DEBUG = True

if os.environ.get('ENV', '') == 'test':
    DATABASES['default'] = {'ENGINE': 'django.db.backends.sqlite3'}
