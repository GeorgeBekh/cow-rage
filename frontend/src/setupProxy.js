const proxy = require('http-proxy-middleware');

const target = 'http://python:8080/'

module.exports = function(app) {
  app.use(proxy('/api', { target }));
  app.use(proxy('/admin', { target }));
  app.use(proxy('/static', { target }));
};
