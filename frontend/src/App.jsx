import React, { Component } from 'react'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'
import { fetchProjectsIfNeeded } from './actions'
import rootReducer from './reducers'
import { Provider } from 'react-redux'

import logo from './logo.svg'
import './App.css'
import List from './List'

const loggerMiddleware = createLogger()

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  )
)

store
  .dispatch(fetchProjectsIfNeeded())

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <List/>
      </Provider>
    )
  }
}

export default App
