import fetch from 'cross-fetch'

function receiveProjects(json) {
  return {
    type: 'RECEIVE_PROJECTS',
    projects: json,
    receivedAt: Date.now()
  }
}

function requestProjects() {
  return {
    type: 'REQUEST_PROJECTS'
  }
}

// THUNK
function fetchProjects() {
  return dispatch => {
    dispatch(requestProjects())
    return fetch('/api/projects/')
      .then(response => response.json())
      .then(json => dispatch(receiveProjects(json)))
  }
}

function shouldFetchProjects(state) {
  const projects = state.projects

  if (!projects) {
    return true
  } else if (projects.isFetching) {
    return false
  } else {
    return projects.didInvalidate
  }
}

// THUNK
export function fetchProjectsIfNeeded(subreddit) {
  return (dispatch, getState) => {
    if (shouldFetchProjects(getState())) {
      return dispatch(fetchProjects())
    } else {
      return Promise.resolve()
    }
  }
}
