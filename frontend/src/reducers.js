import { combineReducers } from 'redux'

function projects(
  state = {
    isFetching: false,
    didInvalidate: true,
    items: []
  },
  action
) {
  switch (action.type) {
    case 'REQUEST_PROJECTS':
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case 'RECEIVE_PROJECTS':
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        items: action.projects,
        lastUpdated: action.receivedAt
      })
      break
    default:
      return state
  }
}

const rootReducer = combineReducers({
  projects,
})

export default rootReducer
