import React, { Component } from 'react'
import { connect } from 'react-redux'

class List  extends Component {
  render() {
    const { projects, isFetching } = this.props

    if (isFetching) {
      return (<div>Loading...</div>)
    }

    return (
      <div className="App">
        {projects.items.map((project) => {
          let fields = []
          for (let field in project) {
            fields = [...fields, (
              <div key={field}>
                {`${field}: ${project[field]}`}
              </div>
            )]
          }
          return (
            <div key={project.id}>
              {fields}
              <hr/>
            </div>
          )
          })}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isFetching: state.projects.isFetching,
  projects: state.projects,
})

export default connect(
  mapStateToProps
)(List)
